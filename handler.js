"use strict";
const {
  profileUploadsService,
  profileImportService,
} = require("./src/context");
const { httpEndpoint } = require("./src/http/http-endpoint");
const { checkValidation } = require("./src/validation/validate");
const openapi = require("./openapi.json");

module.exports.ReadProfileUploadParameters = httpEndpoint(async () => {
  const presignedPost = profileUploadsService.generateProfileUploadParameters();
  return {
    statusCode: 200,
    body: JSON.stringify(presignedPost),
  };
});

module.exports.CreateProfileImport = httpEndpoint(async (event) => {
  const body = JSON.parse(event.body);
  checkCreateProfileImportRequest(body);

  const createdImport = await profileImportService.createProfileImport(
    body.uploadId
  );

  return {
    statusCode: 200,
    body: JSON.stringify(createdImport),
  };
});

module.exports.ReadProfileImport = httpEndpoint(async (event) => {
  const uploadId = event.pathParameters.uploadId;
  const withImportedProfile = isReadWithImportedProfile(event);

  const readImport = await profileImportService.readProfileImport(
    uploadId,
    withImportedProfile
  );

  return {
    statusCode: 200,
    body: JSON.stringify(readImport),
  };
});

/**
 * @param {import("aws-lambda").SNSEvent} event
 */
module.exports.ImportProfile = async function (event) {
  console.debug(
    `Profile imports listener has received: ${event.Records.length} records to process`
  );

  for (const record of event.Records) {
    try {
      console.debug(
        `Started processing of record: ${record.Sns.MessageId}`,
        record
      );
      const { uploadId } = JSON.parse(record.Sns.Message);
      await profileImportService.importProfile(uploadId);
    } catch (err) {
      console.error(
        `Processing of the record: ${record.Sns.MessageId} has failed:`,
        err
      );
    }
  }
};

function isReadWithImportedProfile(event) {
  return (
    event.queryStringParameters &&
    event.queryStringParameters["with-imported-profile"] === "true"
  );
}

function checkCreateProfileImportRequest(payload) {
  checkValidation(
    payload,
    openapi.components.schemas.CreateProfileImportRequest
  );
}
