# Lindin Profiles

Lindin Profiles in an application to convert Linkedin HTML profile pages into JSON files.

## Security

All endpoints are secured with AWS API Gateway key.

One key is configured to be generated automatically along with the deployment.


## Usage

Firstly, the API client has to fetch HTML file upload parameters

```
GET /profile-uploads
```

In the response, the client receives `uploadId`, URL address and form fields needed to
perform multipart upload to AWS S3 bucket.

After successful upload, the client has to start profile import with invocation:

```
POST /profile-imports
{
  "uploadId": ...
}
```

The created import will be in status `ready`, the client can read import details with endpoint:

```
GET /profiles-imports/{uploadId}
```

As soon as the import status in the response is `done`, the imported LinkedIn profile
can be read with:

```
GET /profiles-imports/{uploadId}?with-imported-profile=true
```


## Infrastructure
```plantuml
@startuml
!define AWSPuml https://raw.githubusercontent.com/awslabs/aws-icons-for-plantuml/v7.0/dist

!include <awslib/AWSSimplified>

!includeurl AWSPuml/General/Users.puml
!includeurl AWSPuml/Mobile/APIGateway.puml
!includeurl AWSPuml/Compute/Lambda.puml
!includeurl AWSPuml/Database/DynamoDB.puml
!includeurl AWSPuml/ApplicationIntegration/SNS.puml
!includeurl AWSPuml/Storage/SimpleStorageServiceS3.puml

left to right direction


APIGateway(api, "API Gateway", "user votes")

Lambda(readProfileUploadParameters, "ReadProfileUploadParameters",)
Lambda(createProfileImport, "CreateProfileImport", "")
Lambda(readProfileImport, "ReadProfileImport", "")
Lambda(importProfile, "ImportProfile", "")
SimpleStorageServiceS3(s3, "Bucket", ,)


DynamoDB(db, "DynamoDB", "")
SNS(sns, "OrdersTopic", "")



api --> readProfileUploadParameters
api --> createProfileImport
api --> readProfileImport

readProfileUploadParameters -> s3

createProfileImport --> db
createProfileImport --> sns
createProfileImport --> s3

readProfileImport --> db

sns --> importProfile
importProfile --> db
importProfile --> s3
@enduml
```

## License

Unlicensed
