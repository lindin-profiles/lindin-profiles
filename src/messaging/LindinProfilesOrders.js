/**
 * @typedef {LindinProfilesOrders} LindinProfilesOrders
 */
class LindinProfilesOrders {
  /**
   *
   * @param {import("aws-sdk").SNS} sns
   * @param {import("../configs/config-types").SnsConfig} snsConfig
   */
  constructor(sns, snsConfig) {
    this.sns = sns;
    this.snsConfig = snsConfig;
  }

  sendProfileImport(uploadId) {
    const message = toProfileImportOrderMessage(uploadId);
    console.debug(
      `Sending profile import order message to: ${this.snsConfig.ordersTopicArn}`,
      message
    );

    return this.sns
      .publish({
        TopicArn: this.snsConfig.ordersTopicArn,
        Message: JSON.stringify(message),
      })
      .promise();
  }
}

function toProfileImportOrderMessage(uploadId) {
  return { uploadId };
}

module.exports = { LindinProfilesOrders };
