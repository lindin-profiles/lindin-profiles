const AWS = require("aws-sdk");
const configs = require("./configs");
const { LindinProfilesOrders } = require("./messaging/LindinProfilesOrders");
const {
  LinkedinProfileAnalyzer,
} = require("./profile-import/analyzer/LinkedinProfileAnalyzer");
const {
  LinkedinProfiles,
} = require("./profile-import/analyzer/LinkedinProfiles");
const {
  ProfileImportRepository,
} = require("./profile-import/ProfileImportRepository");
const {
  ProfileImportService,
} = require("./profile-import/ProfileImportService");
const {
  ProfileUploadsOperations,
} = require("./profile-uploads/ProfileUploadsOperations");
const {
  ProfileUploadsService,
} = require("./profile-uploads/ProfileUploadsService");

const s3 = new AWS.S3();
const ddb = new AWS.DynamoDB.DocumentClient();
const sns = new AWS.SNS();

const config = (() => {
  return configs[process.env.STAGE] || configs.dev;
})();

const profileUploadsService = new ProfileUploadsService(s3, config.s3);
const linkedinProfiles = new LinkedinProfiles(new LinkedinProfileAnalyzer());
const profileImportRepository = new ProfileImportRepository(
  ddb,
  config.database
);
const profileUploadsOperations = new ProfileUploadsOperations(s3, config.s3);
const lindinProfilesOrders = new LindinProfilesOrders(sns, config.sns);

const profileImportService = new ProfileImportService(
  profileUploadsOperations,
  profileImportRepository,
  linkedinProfiles,
  lindinProfilesOrders
);

module.exports = {
  profileUploadsService,
  profileImportService,
};
