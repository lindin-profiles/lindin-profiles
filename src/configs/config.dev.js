/**@typedef { import('./config-types').Config } Config */

/** @type {Config} */
module.exports = {
  s3: {
    bucket: "lindinprofiles-dev-lindinprofilesbucket-1u1l0lxlx8duh",
  },
  database: {
    table: "lindinprofiles-dev-LindinProfilesTable-1LQ00YTN89KTF",
  },
  sns: {
    ordersTopicArn:
      "arn:aws:sns:eu-west-1:462027713001:lindinprofiles-dev-OrdersTopic",
  },
};
