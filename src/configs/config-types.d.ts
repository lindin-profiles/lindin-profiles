export interface Config {
  s3: S3Config;
  database: DatabaseConfig;
  sns: SnsConfig;
}

export interface S3Config {
  bucket: string;
}

export interface DatabaseConfig {
  table: string;
}

export interface SnsConfig {
  ordersTopicArn: string;
}
