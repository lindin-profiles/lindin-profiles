/**
 * @typedef { import('aws-lambda').APIGatewayEvent } APIGatewayEvent
 * @typedef { import('aws-lambda').APIGatewayProxyResult } APIGatewayProxyResult
 * @typedef { (event: APIGatewayEvent) => Promise<APIGatewayProxyResult> } HttpHandler
 */

const { UserError } = require("../UserError");
const { ValidationError } = require("../validation/ValidationError");
const { nanoid } = require("nanoid");
const { ResourceNotFoundError } = require("../errors/ResourceNotFoundError");

/**
 * @param {HttpHandler} handler
 */
function httpEndpoint(handler) {
  return (event) => advisedHttpEndpoint(event, handler);
}

/**
 * @param {APIGatewayEvent} event
 * @param {HttpHandler} handler
 * @returns {Promise<APIGatewayProxyResult>}
 */
function advisedHttpEndpoint(event, handler) {
  console.debug(`Invoking endpoint: ${event.httpMethod} ${event.path}`);
  return handler(event)
    .then((response) => response, handleEndpointError)
    .then(adviseCorsHeaders);
}

function adviseCorsHeaders(response) {
  return {
    ...response,
    headers: {
      ...response.headers,
      "Access-Control-Allow-Origin": "*",
      "Access-Control-Allow-Methods": "GET, POST, PUT, PATCH, DELETE, OPTIONS",
    },
  };
}

/**
 * @param {Error} err
 * @returns {Promise<APIGatewayProxyResult>}
 */
function handleEndpointError(err) {
  if (err instanceof UserError) {
    const responseBody = {
      code: err.code,
      errorId: nanoid(),
    };
    console.error(err, { errorId: responseBody.errorId });
    return Promise.resolve({
      statusCode: 422,
      body: JSON.stringify(responseBody),
    });
  }
  if (err instanceof ResourceNotFoundError) {
    const responseBody = {
      code: "resource-not-found",
      resource: err.resource,
      identifier: err.identifier,
      errorId: nanoid(),
    };
    console.error(err, { errorId: responseBody.errorId });
    return Promise.resolve({
      statusCode: 404,
      body: JSON.stringify(responseBody),
    });
  }
  if (err instanceof ValidationError) {
    const responseBody = {
      code: "validation-error",
      violations: err.violations,
    };
    return Promise.resolve({
      statusCode: 400,
      body: JSON.stringify(responseBody),
    });
  }

  const responseBody = {
    code: "internal-server-error",
    errorId: nanoid(),
  };
  console.error(err, { errorId: responseBody.errorId });
  return Promise.resolve({
    statusCode: 500,
    body: JSON.stringify(responseBody),
  });
}

module.exports = { httpEndpoint };
