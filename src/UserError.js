class UserError extends Error {
  constructor(code, err) {
    super("User error with code: " + code);
    this.code = code;
    this.stack = !!err
      ? [this.stack, "Caused by: " + err.stack].join("\n")
      : this.stack;
  }
}

module.exports = { UserError };
