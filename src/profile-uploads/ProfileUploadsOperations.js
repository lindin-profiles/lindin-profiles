/**
 * @typedef {import('aws-sdk').S3} S3
 * @typedef {import('../configs/config-types').S3Config} S3Config
 * @typedef {ProfileUploadsOperations} ProfileUploadsOperations
 */

class ProfileUploadsOperations {
  /**
   * @param {S3} s3
   * @param {S3Config} s3Config
   */
  constructor(s3, s3Config) {
    this.s3 = s3;
    this.s3Config = s3Config;
  }

  /**
   * @param {string} uploadId
   */
  acceptIncomingProfileUpload(uploadId) {
    console.debug("Accepting incoming profile upload with id: " + uploadId);
    return this.s3
      .copyObject({
        Bucket: this.s3Config.bucket,
        CopySource: [
          this.s3Config.bucket,
          toIncomingProfileUploadKey(uploadId),
        ].join("/"),
        Key: toProfileUploadKey(uploadId),
      })
      .promise();
  }

  /**
   *
   * @param {string} uploadId
   * @returns {Promise<boolean>}
   */
  isIncomingProfileUploadPresent(uploadId) {
    console.debug(
      `Checking if incoming profile upload with id: ${uploadId} is present.`
    );
    return this.s3
      .headObject({
        Bucket: this.s3Config.bucket,
        Key: toIncomingProfileUploadKey(uploadId),
      })
      .promise()
      .then(
        () => true,
        (err) => (err.code === "NotFound" ? false : Promise.reject(err))
      );
  }

  async downloadLinkedinProfilePage(uploadId) {
    console.debug("Downloading linkedin profile page for upload: " + uploadId);
    const s3Key = toProfileUploadKey(uploadId);
    const response = await this.s3
      .getObject({
        Bucket: this.s3Config.bucket,
        Key: s3Key,
        ResponseContentType: "text/html",
      })
      .promise();

    console.debug(
      `Successfully downloaded linkedin profile page for upload: ${uploadId}. Read ${response.Body.length} bytes`
    );

    return {
      filename: s3Key,
      content: response.Body,
    };
  }
}

function toIncomingProfileUploadKey(uploadId) {
  return `incoming-uploads/${uploadId}/profile.html`;
}

function toProfileUploadKey(uploadId) {
  return `profiles/${uploadId}/profile.html`;
}

module.exports = { ProfileUploadsOperations };
