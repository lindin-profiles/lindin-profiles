const { S3 } = require("aws-sdk");
const { ProfileUploadsService } = require("../ProfileUploadsService");

const profileUploadsService = new ProfileUploadsService(
  new S3({ credentials: { accessKeyId: "KEY1", secretAccessKey: "SECRET" } }),
  {
    bucket: "some-bucket",
  }
);

test("Test generate profile upload parameters", () => {
  // given & when:
  const uploadParameters = profileUploadsService.generateProfileUploadParameters();

  // then:
  expect(uploadParameters.url).toEqual("https://s3.amazonaws.com/some-bucket");
  expect(uploadParameters.uploadId).not.toBeNull();

  const allFieldNames = uploadParameters.fields
    .map((field) => field.name)
    .sort();
  expect(allFieldNames).toMatchInlineSnapshot(`
    Array [
      "Policy",
      "X-Amz-Algorithm",
      "X-Amz-Credential",
      "X-Amz-Date",
      "X-Amz-Signature",
      "bucket",
      "key",
    ]
  `);
});
