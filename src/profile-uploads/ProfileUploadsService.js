/**
 * @typedef { import('aws-sdk').S3 } S3
 * @typedef { import('../configs').S3Config } S3Config
 */

const { nanoid } = require("nanoid");

const _5MB = 1024 * 1024 * 10;
class ProfileUploadsService {
  /**
   * @param {S3} s3
   * @param {S3Config} s3Config
   */
  constructor(s3, s3Config) {
    this.s3 = s3;
    this.s3Config = s3Config;
  }

  generateProfileUploadParameters() {
    console.log(
      "Generating profile upload parameters to bucket: " + this.s3Config.bucket
    );
    const uploadId = nanoid();
    const s3Key = `incoming-uploads/${uploadId}/profile.html`;

    const presignedPost = this.s3.createPresignedPost({
      Bucket: this.s3Config.bucket,
      Conditions: [
        ["eq", "$key", s3Key],
        ["content-length-range", 0, _5MB],
      ],
    });

    return {
      uploadId,
      url: presignedPost.url,
      fields: Object.entries(presignedPost.fields)
        .map(([name, value]) => ({
          name,
          value,
        }))
        .concat([
          {
            value: s3Key,
            name: "key",
          },
        ]),
    };
  }
}

module.exports = { ProfileUploadsService };
