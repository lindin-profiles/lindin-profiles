/**
 * @typedef { LinkedinProfileAnalyzer } LinkedinProfileAnalyzer
 */
const htmlParser = require("node-html-parser");
const htmlEntities = require("html-entities");

class LinkedinProfileAnalyzer {
  /**
   * Analyzer method for LinkedIn html profiles files.
   *
   * LinkedIn uses HTML "code" nodes to optimize page rendering.
   * Following method reads all of those fragments and combines
   * them into list of the records.
   *
   * @param {string} fileContent
   */
  analyzeProfile(profilePageHtml) {
    const allHtmlEntities = new htmlEntities.AllHtmlEntities();
    const dom = htmlParser.parse(profilePageHtml);
    const codeNodes = dom.querySelectorAll("code");

    // JSONs in html files contain all special characters as html entities.
    // Profile data are present only in JSONs whith "data" attribute
    const capturedJsons = codeNodes
      .map((node) => allHtmlEntities.decode(node.innerText.trim()))
      .map((text) => lenientlyParseJson(text));

    const database = capturedJsons
      .map((capturedItem) => toRootRecord(capturedItem))
      .reduce((left, right) => left.concat(right), []);

    console.debug(
      `Successfully analyzed linkedin profile. Found ${database.length} records`
    );
    return database;
  }
}

function lenientlyParseJson(text) {
  try {
    return JSON.parse(text);
  } catch (err) {
    return { raw: text };
  }
}

/**
 * @returns {Array}
 */
function toRootRecord(item) {
  const self = item.data ? toEntry(item.data) : [];
  const included = item.included
    ? item.included
        .map((includedItem) => toEntry(includedItem))
        .reduce((left, right) => left.concat(right), [])
    : [];
  return self.concat(included);
}

/**
 * @returns {Array}
 */
function toEntry(item) {
  const self =
    item.$type && item.$type !== "com.linkedin.restli.common.CollectionResponse"
      ? [item]
      : [];
  const collectionRelations =
    item.$type === "com.linkedin.restli.common.CollectionResponse"
      ? [
          {
            entityUrn: item.entityUrn,
            "*elements": item["*elements"] || [],
          },
        ]
      : [];
  const included = item.included
    ? item.included
        .map((includedItem) => toEntry(includedItem))
        .reduce((left, right) => left.concat(right), [])
    : [];
  return self.concat(collectionRelations).concat(included);
}

module.exports = { LinkedinProfileAnalyzer };
