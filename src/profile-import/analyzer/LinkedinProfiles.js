/**
 * @typedef { LinkedinProfiles } LinkedinProfiles
 */

const { UserError } = require("../../UserError");
const {
  findProfileRecord,
  findPersonalInformation,
  findEmploymentHistory,
  findSkills,
  findLanguages,
  findEducations,
  findCertifications,
  findCourses,
  findProjects,
} = require("./LinkedinProfileQueries");

class LinkedinProfiles {
  /**
   * @param {import("./LinkedinProfileAnalyzer").LinkedinProfileAnalyzer} linkedinProfileAnalyzer
   */
  constructor(linkedinProfileAnalyzer) {
    this.linkedinProfileAnalyzer = linkedinProfileAnalyzer;
  }

  extractLinkedinProfile(profilePageHtml) {
    console.debug("Extracting Linkedin profile from the profile HTML page");
    const database = this.linkedinProfileAnalyzer.analyzeProfile(
      profilePageHtml
    );
    const profileRecord = findProfileRecord(database);
    if (!profileRecord) {
      throw new UserError("profile-database-missing-personal-record");
    }

    const personalInformation = findPersonalInformation(
      profileRecord,
      database
    );

    const employmentHistory = findEmploymentHistory(profileRecord, database);
    const skills = findSkills(profileRecord, database);
    const languages = findLanguages(profileRecord, database);
    const educations = findEducations(profileRecord, database);
    const certifications = findCertifications(profileRecord, database);
    const courses = findCourses(profileRecord, database);
    const projects = findProjects(profileRecord, database);

    return {
      personalInformation,
      employmentHistory,
      skills,
      languages,
      educations,
      certifications,
      courses,
      projects,
    };
  }
}
module.exports = {
  LinkedinProfiles,
};
