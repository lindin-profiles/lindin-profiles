function findProfileRecord(database) {
  return database
    .filter(
      (record) =>
        record.$type === "com.linkedin.voyager.dash.identity.profile.Profile"
    )
    .find((profileRecord) => !!profileRecord["*profileSkills"]);
}

function findPersonalInformation(profileRecord, database) {
  const pictures = profileRecord.profilePicture
    ? toPictures(profileRecord)
    : [];
  const industry = findProfileIndustry(profileRecord, database);
  return {
    firstName: profileRecord.firstName,
    lastName: profileRecord.lastName,
    location: {
      countryCode: profileRecord.location.countryCode,
    },
    headline: profileRecord.headline,
    publicIdentifier: profileRecord.publicIdentifier,
    pictures,
    industry,
  };
}

function toPictures(profileRecord) {
  return profileRecord.profilePicture.displayImageReference.vectorImage.artifacts.map(
    (artifact) => ({
      width: artifact.width,
      height: artifact.height,
      url:
        profileRecord.profilePicture.displayImageReference.vectorImage.rootUrl +
        artifact.fileIdentifyingUrlPathSegment,
    })
  );
}

function findEmploymentHistory(profileRecord, database) {
  const employmentItemsRecords = fetchCollection(
    profileRecord["*profilePositionGroups"],
    database
  );

  return employmentItemsRecords.map((employmentRecord) => {
    const occupations = findOccupations(employmentRecord, database);
    const company = findCompany(employmentRecord, database);
    return {
      period: toPeriod(employmentRecord.dateRange),
      company,
      occupations,
    };
  });
}

function fetchCollection(collectionUrn, database) {
  const collection = database.find(
    (record) => record.entityUrn === collectionUrn
  );

  const collectionRecords = collection["*elements"].map((urn) =>
    database.find((record) => record.entityUrn === urn)
  );
  return collectionRecords;
}

function findSkills(profileRecord, database) {
  const skillsRecords = fetchCollection(
    profileRecord["*profileSkills"],
    database
  );
  return skillsRecords.map((skillRecord) => ({ name: skillRecord.name }));
}

function findLanguages(profileRecord, database) {
  const languageRecords = fetchCollection(
    profileRecord["*profileLanguages"],
    database
  );
  return languageRecords.map((languageRecord) => ({
    name: languageRecord.name,
    proficiency: languageRecord.proficiency,
  }));
}

function findCertifications(profileRecord, database) {
  const certificationRecords = fetchCollection(
    profileRecord["*profileCertifications"],
    database
  );
  return certificationRecords.map((ceritifationRecord) => ({
    name: ceritifationRecord.name,
    authority: ceritifationRecord.authority,
    period: toPeriod(ceritifationRecord.dateRange),
    licenseNumber: ceritifationRecord.licenseNumber,
  }));
}

function findCourses(profileRecord, database) {
  const coursesRecords = fetchCollection(
    profileRecord["*profileCourses"],
    database
  );
  return coursesRecords.map((courseRecord) => ({
    number: courseRecord.number,
    name: courseRecord.name,
  }));
}

function findEducations(profileRecord, database) {
  const educationRecords = fetchCollection(
    profileRecord["*profileEducations"],
    database
  );
  return educationRecords.map((educationRecord) => {
    const schoolRecord = database.find(
      (record) => record.entityUrn === educationRecord["*school"]
    );
    return {
      period: toPeriod(educationRecord.dateRange),
      school: {
        name: schoolRecord.name,
        urn: schoolRecord.entityUrn,
      },
      fieldOfStudy: {
        name: educationRecord.fieldOfStudy,
        urn: educationRecord.fieldOfStudyUrn,
      },
      degree: {
        name: educationRecord.degreeName,
        urn: educationRecord.degreeUrn,
      },
    };
  });
}

function findProfileIndustry(profileRecord, database) {
  const industryUrn = profileRecord["*industry"];
  if (!industryUrn) {
    return null;
  }

  const industryRecord = database.find(
    (record) => record.entityUrn === industryUrn
  );
  return {
    urn: industryUrn,
    name: industryRecord.name,
  };
}

function findCompany(employmentRecord, database) {
  const companyUrn = employmentRecord["*company"];
  if (!companyUrn) {
    return {
      name: employmentRecord.companyName,
      urn: null,
      industries: [],
    };
  }
  const companyRecords = database.filter(
    (record) =>
      record.entityUrn === companyUrn &&
      record.$type === "com.linkedin.voyager.dash.organization.Company"
  );
  if (!companyRecords) {
    return {
      name: employmentRecord.companyName,
      urn: companyUrn,
      industries: [],
    };
  }

  const industries = companyRecords
    .map((companyRecord) => findCompanyIndustries(companyRecord, database))
    .reduce((left, right) => left.concat(right), []);
  return {
    name: employmentRecord.companyName,
    urn: companyUrn,
    industries,
  };
}

function findCompanyIndustries(companyRecord, database) {
  if (!companyRecord.industryUrns) {
    return [];
  }
  const industryRecords = companyRecord.industryUrns.map((industryUrn) =>
    database.find((record) => record.entityUrn === industryUrn)
  );
  return industryRecords.map((industryRecord) => ({
    urn: industryRecord.entityUrn,
    name: industryRecord.name,
  }));
}

function findProjects(profileRecord, database) {
  const projectsRecords = fetchCollection(
    profileRecord["*profileProjects"],
    database
  );
  return projectsRecords.map((projectRecord) => ({
    period: toPeriod(projectRecord.dateRange),
    title: projectRecord.title,
    url: projectRecord.url,
    description: projectRecord.description,
  }));
}

function findOccupations(employmentRecord, database) {
  const occupationRecords = fetchCollection(
    employmentRecord["*profilePositionInPositionGroup"],
    database
  );

  return occupationRecords.map((occupationRecord) => ({
    period: toPeriod(occupationRecord.dateRange),
    company: {
      name: occupationRecord.companyName,
      urn: occupationRecord.companyUrn,
    },
    title: occupationRecord.title,
  }));
}

function toPeriod(dateRange) {
  if (!dateRange) {
    return {};
  }
  const start = !!dateRange.start
    ? { year: dateRange.start.year, month: dateRange.start.month }
    : null;
  const end = !!dateRange.end
    ? { year: dateRange.end.year, month: dateRange.end.month }
    : null;
  return { start, end };
}

module.exports = {
  findProfileRecord,
  findPersonalInformation,
  findEmploymentHistory,
  findSkills,
  findLanguages,
  findEducations,
  findCertifications,
  findCourses,
  findProjects,
};
