const { LinkedinProfileAnalyzer } = require("../LinkedinProfileAnalyzer");
const { LinkedinProfiles } = require("../LinkedinProfiles");
const fs = require("fs");

const linkedinProfiles = new LinkedinProfiles(new LinkedinProfileAnalyzer());

["Maciej Krzeminski _ LinkedIn.htm", "Linus Torvalds _ LinkedIn.htm"].map(
  (filename) =>
    test("Test extract full linkedin profile: " + filename, async () => {
      // given:
      const profilePageHtml = await fs.promises.readFile(
        __dirname + "/linkedin-profile-pages/" + filename,
        { encoding: "utf-8" }
      );

      // when:
      const linkedinProfile = linkedinProfiles.extractLinkedinProfile(
        profilePageHtml
      );

      // then:
      expect(linkedinProfile).toMatchSnapshot();
    })
);
