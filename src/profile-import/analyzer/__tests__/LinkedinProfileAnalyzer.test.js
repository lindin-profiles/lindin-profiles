const { LinkedinProfileAnalyzer } = require("../LinkedinProfileAnalyzer");
const fs = require("fs");

["Linus Torvalds _ LinkedIn.htm", "Maciej Krzeminski _ LinkedIn.htm"].map(
  (filename) => {
    test("Test analyze profile: " + filename, async () => {
      // given:
      const html = await fs.promises.readFile(
        __dirname + "/linkedin-profile-pages/" + filename,
        { encoding: "utf-8" }
      );

      // when:
      const database = new LinkedinProfileAnalyzer().analyzeProfile(html);

      // then:
      const allTypes = database.map((record) =>
        record.$type ? record.$type : "Collection"
      );

      const summary = allTypes.reduce((downstream, item) => {
        downstream[item] = downstream[item] || 0;
        downstream[item]++;
        return downstream;
      }, {});

      expect(summary).toMatchSnapshot();
    });
  }
);
