const {
  findPersonalInformation,
  findProfileRecord,
  findSkills,
  findEmploymentHistory,
  findCertifications,
  findCourses,
  findLanguages,
  findEducations,
  findProjects,
} = require("../LinkedinProfileQueries");

const maciejKrzeminskiProfile = require("./MaciejKrzeminski-profile-database.json");
const linusTorvaldsProfile = require("./LinusTorvalds-profile-database.json");

test("Test find personal information", () => {
  const profileRecord = findProfileRecord(maciejKrzeminskiProfile);

  const personalInformation = findPersonalInformation(
    profileRecord,
    maciejKrzeminskiProfile
  );

  expect(personalInformation).toMatchInlineSnapshot(`
    Object {
      "firstName": "Maciej",
      "headline": "IT Manager at BlueSoft",
      "industry": Object {
        "name": "Technologie i usługi informatyczne",
        "urn": "urn:li:fsd_industry:96",
      },
      "lastName": "Krzemiński",
      "location": Object {
        "countryCode": "pl",
      },
      "pictures": Array [
        Object {
          "height": 100,
          "url": "https://media-exp1.licdn.com/dms/image/C5603AQFVVSrw66EgAQ/profile-displayphoto-shrink_100_100/0?e=1612396800&v=beta&t=bHImZDDztbbUb0bWY7eh4X7WfgLCmozWKMzGizwGSW4",
          "width": 100,
        },
        Object {
          "height": 200,
          "url": "https://media-exp1.licdn.com/dms/image/C5603AQFVVSrw66EgAQ/profile-displayphoto-shrink_200_200/0?e=1612396800&v=beta&t=fulMlRjWxRbUcNXXc_LFYSAw1p_TCg3_l2nxp6VXfPw",
          "width": 200,
        },
        Object {
          "height": 400,
          "url": "https://media-exp1.licdn.com/dms/image/C5603AQFVVSrw66EgAQ/profile-displayphoto-shrink_400_400/0?e=1612396800&v=beta&t=fTCBi00GJufpyzCY7HLt124iwX_kZGQbwoNKoU8jHcM",
          "width": 400,
        },
        Object {
          "height": 800,
          "url": "https://media-exp1.licdn.com/dms/image/C5603AQFVVSrw66EgAQ/profile-displayphoto-shrink_800_800/0?e=1612396800&v=beta&t=1uA43sFjHQKYw7RzpMyIqCEy3ColhGoYT0XDxbsWsS0",
          "width": 800,
        },
      ],
      "publicIdentifier": "maciej-krzemiński-7598745b",
    }
  `);
});

test("Test find job positions", () => {
  const profileRecord = findProfileRecord(maciejKrzeminskiProfile);

  const employmentHistory = findEmploymentHistory(
    profileRecord,
    maciejKrzeminskiProfile
  );

  expect(employmentHistory).toMatchSnapshot();
});

test("Test find skills", () => {
  const profileRecord = findProfileRecord(maciejKrzeminskiProfile);

  const skills = findSkills(profileRecord, maciejKrzeminskiProfile);

  expect(skills).toMatchSnapshot();
});
test("Test find certifications", () => {
  const profileRecord = findProfileRecord(maciejKrzeminskiProfile);

  const certifications = findCertifications(
    profileRecord,
    maciejKrzeminskiProfile
  );

  expect(certifications).toMatchInlineSnapshot(`
    Array [
      Object {
        "authority": "Project Management Institute",
        "licenseNumber": "1761554",
        "name": "PMP",
        "period": Object {
          "end": Object {
            "month": 10,
            "year": 2017,
          },
          "start": Object {
            "month": 10,
            "year": 2014,
          },
        },
      },
    ]
  `);
});

test("Test find courses", () => {
  const profileRecord = findProfileRecord(maciejKrzeminskiProfile);

  const courses = findCourses(profileRecord, maciejKrzeminskiProfile);

  expect(courses).toMatchInlineSnapshot(`
    Array [
      Object {
        "name": "AR System 6.x Performance Tuning and Troubleshooting ",
        "number": null,
      },
      Object {
        "name": "Information Technology Infrastructure Library (ITIL)",
        "number": null,
      },
      Object {
        "name": "PM BOK",
        "number": null,
      },
      Object {
        "name": "Podstawy modelowania procesów z użyciem BPMN",
        "number": null,
      },
      Object {
        "name": "Preparation course for PMP Examination",
        "number": null,
      },
      Object {
        "name": "System Zarządzania bezpieczeństwem informacji zgodnie z PN-ISO/IEC 27001",
        "number": "50/ISMS/2008",
      },
      Object {
        "name": "Zarządzanie projektami według metodyki Prince2",
        "number": null,
      },
    ]
  `);
});

test("Test find languages", () => {
  const profileRecord = findProfileRecord(maciejKrzeminskiProfile);

  const languages = findLanguages(profileRecord, maciejKrzeminskiProfile);

  expect(languages).toMatchInlineSnapshot(`
    Array [
      Object {
        "name": "English",
        "proficiency": "PROFESSIONAL_WORKING",
      },
      Object {
        "name": "Polish",
        "proficiency": "NATIVE_OR_BILINGUAL",
      },
      Object {
        "name": "Spanish",
        "proficiency": "ELEMENTARY",
      },
    ]
  `);
});
test("Test find educations", () => {
  const profileRecord = findProfileRecord(maciejKrzeminskiProfile);

  const educations = findEducations(profileRecord, maciejKrzeminskiProfile);

  expect(educations).toMatchSnapshot();
});

test("Test find projects", () => {
  const profileRecord = findProfileRecord(linusTorvaldsProfile);

  const projects = findProjects(profileRecord, linusTorvaldsProfile);

  expect(projects).toMatchInlineSnapshot(`
    Array [
      Object {
        "description": "Multi-platform divelog software in C",
        "period": Object {
          "end": null,
          "start": Object {
            "month": 8,
            "year": 2011,
          },
        },
        "title": "Subsurface",
        "url": "http://subsurface.hohndel.org/",
      },
      Object {
        "description": "Distributed version control system",
        "period": Object {
          "end": null,
          "start": Object {
            "month": 4,
            "year": 2005,
          },
        },
        "title": "Git",
        "url": "http://git-scm.com/",
      },
      Object {
        "description": null,
        "period": Object {
          "end": null,
          "start": Object {
            "month": 4,
            "year": 1991,
          },
        },
        "title": "Linux",
        "url": "http://www.kernel.org/",
      },
    ]
  `);
});
