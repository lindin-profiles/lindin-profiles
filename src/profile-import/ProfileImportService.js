/**
 * @typedef {import("../profile-uploads/ProfileUploadsOperations").ProfileUploadsOperations} ProfileUploadsOperations
 * @typedef {import("./ProfileImportRepository").ProfileImportRepository} ProfileImportRepository
 * @typedef {import("./analyzer/LinkedinProfiles").LinkedinProfiles} LinkedinProfiles
 * @typedef {import("../messaging/LindinProfilesOrders").LindinProfilesOrders} LindinProfilesOrders
 */
const { UserError } = require("../UserError");

class ProfileImportService {
  /**
   * @param {ProfileUploadsOperations} profileUploadsOperations
   * @param {ProfileImportRepository} profileImportRepository
   * @param {LinkedinProfiles} linkedinProfiles
   * @param {LindinProfilesOrders} lindinProfilesOrders
   */
  constructor(
    profileUploadsOperations,
    profileImportRepository,
    linkedinProfiles,
    lindinProfilesOrders
  ) {
    this.profileUploadsOperations = profileUploadsOperations;
    this.profileImportRepository = profileImportRepository;
    this.linkedinProfiles = linkedinProfiles;
    this.lindinProfilesOrders = lindinProfilesOrders;
  }

  /**
   * @param {string} uploadId
   */
  async createProfileImport(uploadId) {
    console.log("Creating profile import with upload id: " + uploadId);
    const isIncomingUploadPresent = await this.profileUploadsOperations.isIncomingProfileUploadPresent(
      uploadId
    );
    if (!isIncomingUploadPresent) {
      throw new UserError("incoming-profile-upload-missing");
    }
    const profileImport = {
      uploadId,
      status: "ready",
      createdAt: new Date().toISOString(),
    };
    await this.profileImportRepository.save(profileImport);
    await this.profileUploadsOperations.acceptIncomingProfileUpload(uploadId);
    await this.lindinProfilesOrders.sendProfileImport(uploadId);
    return profileImport;
  }
  /**
   * @param {string} uploadId
   * @param {boolean} withImportedProfile
   */
  async readProfileImport(uploadId, withImportedProfile = false) {
    console.log("Reading profile import with upload id: " + uploadId);

    const profileImport = await this.profileImportRepository.find(uploadId);
    if (!withImportedProfile) {
      return profileImport;
    }
    const importedProfile = await this.profileImportRepository.tryFindImportedProfile(
      uploadId
    );
    return {
      ...profileImport,
      importedProfile,
    };
  }

  /**
   * @param {string} uploadId
   */
  async importProfile(uploadId) {
    console.log("Importing Linkedin profile with upload id: " + uploadId);
    const foundProfileImport = await this.profileImportRepository.find(
      uploadId
    );
    if (!isProfileImportStatusEligibile(foundProfileImport)) {
      throw new UserError("illegal-profile-import-status");
    }

    await this.profileImportRepository.updateImportStatus({
      ...foundProfileImport,
      status: "pending",
    });

    try {
      const downloadedProfilePage = await this.profileUploadsOperations.downloadLinkedinProfilePage(
        uploadId
      );
      const extractedProfile = this.linkedinProfiles.extractLinkedinProfile(
        downloadedProfilePage.content.toString("utf-8")
      );
      await this.profileImportRepository.saveImportedProfile(
        uploadId,
        extractedProfile
      );
      console.log(
        "Successfully imported linkedin profile with upload id: " + uploadId
      );
    } catch (err) {
      console.error(
        `Import of the linkedin profile with upload id: ${uploadId} has failed:`,
        err
      );
      await this.profileImportRepository.updateImportStatus({
        ...foundProfileImport,
        status: "error",
      });
    }
  }
}

function isProfileImportStatusEligibile(foundProfileImport) {
  return ["ready", "error"].includes(foundProfileImport.status);
}

module.exports = { ProfileImportService };
