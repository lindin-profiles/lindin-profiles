/**
 * @typedef {import('aws-sdk').DynamoDB.DocumentClient} DynamoDB.DocumentClient
 * @typedef {import('../configs/config-types').DatabaseConfig} DatabaseConfig
 * @typedef {ProfileImportRepository} ProfileImportRepository
 */

const { ResourceNotFoundError } = require("../errors/ResourceNotFoundError");
const { ResourcesConflictError } = require("../errors/ResourcesConflictError");

class ProfileImportRepository {
  /**
   * @param {DynamoDB.DocumentClient} ddb
   * @param {DatabaseConfig} config
   */
  constructor(ddb, config) {
    this.ddb = ddb;
    this.config = config;
  }

  find(id) {
    return this.ddb
      .get({
        TableName: this.config.table,
        Key: {
          gid: `profile-import:${id}`,
          resource: `profile-import`,
        },
      })
      .promise()
      .then((response) => {
        if (!response.Item) {
          return Promise.reject(
            new ResourceNotFoundError("profile-import", id)
          );
        }
        return toProfileImport(response.Item);
      });
  }

  tryFindImportedProfile(id) {
    return this.ddb
      .get({
        TableName: this.config.table,
        Key: {
          gid: `profile-import:${id}`,
          resource: "imported-profile",
        },
      })
      .promise()
      .then((response) => {
        if (!response.Item) {
          return null;
        }
        return response.Item.payload;
      });
  }

  save(profileImport) {
    const record = toProfileImportRecord(profileImport);
    return this.ddb
      .put({
        TableName: this.config.table,
        Key: {
          gid: record.gid,
          resource: `profile-import`,
        },
        Item: record,
        ConditionExpression: "attribute_not_exists(gid)",
      })
      .promise()
      .then(() => record, captureTransactionError);
  }

  updateImportStatus(profileImport) {
    const profileImportRecord = toProfileImportRecord(profileImport);
    return this.ddb
      .update({
        TableName: this.config.table,
        Key: {
          gid: profileImportRecord.gid,
          resource: "profile-import",
        },
        UpdateExpression: "SET #status = :targetStatus",
        ExpressionAttributeNames: {
          "#status": "status",
        },
        ExpressionAttributeValues: {
          ":targetStatus": profileImportRecord.status,
        },
      })
      .promise()
      .then(() => profileImport, captureTransactionError);
  }

  saveImportedProfile(uploadId, profilePayload) {
    const recordGid = toProfileImportGid(uploadId);
    return this.ddb
      .transactWrite({
        TransactItems: [
          {
            Put: {
              TableName: this.config.table,
              Key: {
                gid: recordGid,
                resource: "imported-profile",
              },
              Item: {
                gid: recordGid,
                resource: "imported-profile",
                linkedinIdentifier:
                  profilePayload.personalInformation.publicIdentifier,
                payload: profilePayload,
              },
              ConditionExpression: "attribute_not_exists(gid)",
            },
          },
          {
            Update: {
              TableName: this.config.table,
              Key: {
                gid: recordGid,
                resource: "profile-import",
              },
              UpdateExpression: "SET #status = :targetStatus",
              ExpressionAttributeNames: {
                "#status": "status",
              },
              ExpressionAttributeValues: {
                ":targetStatus": "done",
              },
            },
          },
        ],
      })
      .promise()
      .then(() => profilePayload, captureTransactionError);
  }
}

function toProfileImportRecord(profileImport) {
  return {
    gid: toProfileImportGid(profileImport.uploadId),
    resource: "profile-import",
    uploadId: profileImport.uploadId,
    createdAt: profileImport.createdAt,
    status: profileImport.status,
  };
}

function toProfileImport(record) {
  return {
    uploadId: record.uploadId,
    createdAt: record.createdAt,
    status: record.status,
  };
}

function toProfileImportGid(uploadId) {
  return `profile-import:${uploadId}`;
}

function captureTransactionError(err) {
  if (err.code && err.code === "TransactionCanceledException") {
    console.debug(err);
    return Promise.reject(new ResourcesConflictError("profile-import"));
  }
  return Promise.reject(err);
}

module.exports = { ProfileImportRepository };
