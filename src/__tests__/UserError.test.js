const { UserError } = require("../UserError");

test("Test stacktrace chaining", () => {
  const cause = new Error("Root cause");

  const userError = new UserError("network-transport-layer-failure", cause);

  expect(userError.message).toContain(userError.code);
  expect(userError.stack).toContain(cause.stack);
});
