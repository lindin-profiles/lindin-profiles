/**
 * @typedef { import('ajv').ErrorObject } AjvErrorObject
 */

class ValidationError extends Error {
  /**
   * @param {AjvErrorObject[]} violations
   */
  constructor(violations) {
    super(
      [
        `Validation error. Detected ${violations.length} constraint violation/s.`,
        JSON.stringify(violations.slice(0, 2), undefined, 2),
      ].join(" ")
    );
    this.violations = violations.map((violation) => ({
      constraint: violation.keyword,
      message: violation.message,
      path: violation.schemaPath,
    }));
  }
}

module.exports = { ValidationError };
