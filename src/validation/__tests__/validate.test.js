const testSchema01 = require("./test-schema-01.json");
const { checkValidation } = require("../validate");
const { ValidationError } = require("../ValidationError");

test("Test validate invalid payload", () => {
  // given:
  const payload = {
    prop1: 1,
  };

  // when:
  const invocation = () => checkValidation(payload, testSchema01);

  // then:
  expect(invocation).toThrow(ValidationError);
});

test("Test validate valid payload", () => {
  // given:
  const payload = {
    prop1: "p1",
    prop24: "v3",
  };

  // when:
  const invocation = () => checkValidation(payload, testSchema01);

  // then:
  expect(invocation).toThrowErrorMatchingSnapshot();
});
