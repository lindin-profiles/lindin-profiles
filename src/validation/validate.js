const Ajv = require("ajv");
const { ValidationError } = require("./ValidationError");

function checkValidation(object, schema) {
  const validator = new Ajv().compile(schema);

  const result = validator(object);
  if (typeof result !== "boolean") {
    throw new Error("Unexpected PromiseLike response from validation function");
  }
  if (!result) {
    throw new ValidationError(validator.errors);
  }
}

module.exports = { checkValidation };
