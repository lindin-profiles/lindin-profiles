class ResourceNotFoundError extends Error {
  constructor(resource, identifier) {
    super("Cannot find resource: " + resource + " with id: " + identifier);
    this.resource = resource;
    this.identifier = identifier;
  }
}

module.exports = { ResourceNotFoundError };
