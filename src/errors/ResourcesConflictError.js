class ResourcesConflictError extends Error {
  constructor(resource) {
    super("Conflict of resources: " + resource + " has occurred.");
    this.resource = resource;
  }
}

module.exports = { ResourcesConflictError };
